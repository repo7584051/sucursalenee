﻿using CQRS_MediatR.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CQRS_MediatR.Context
{
    public class SucursalDbContext : DbContext, ISucursalDbContext
    {
        // Add-Migration InitialMigration -Context SucursalDbContext
        //update-database -Context SucursalDbContext
        public SucursalDbContext(DbContextOptions<SucursalDbContext> options) : base(options)
        { }

        public DbSet<Sucursal> Sucursal { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

 
            #region Relationships

            builder.ApplyConfiguration(new SucursalConfig());

            #endregion


        }
        public async Task<int> SaveChangesAsync()
        {
            return await base.SaveChangesAsync();
        }
       
    }
}
