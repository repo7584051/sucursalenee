﻿using CQRS_MediatR.Context;
using CQRS_MediatR.Features.TransactionFeatures.Commands;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS_MediatR
{
    public class CreateSucursalCommandValidator : AbstractValidator<CreateSucursalCommand>
    {
        private readonly ISucursalDbContext _context;
        public CreateSucursalCommandValidator(ISucursalDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            RuleFor(command => command.NombreSucursal)
                .NotEmpty().WithMessage("El nombre de la sucursal no puede estar vacío.")
                .MustAsync(BeUniqueNombreSucursal)
                .WithMessage("Ya existe una sucursal con este nombre.");
        }


        private async Task<bool> BeUniqueNombreSucursal(string nombreSucursal, CancellationToken cancellationToken)
        {
            return await _context.Sucursal.AllAsync(s => s.NombreSucursal != nombreSucursal );
        }
    }
}