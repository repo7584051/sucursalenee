﻿using CQRS_MediatR.Context;
using CQRS_MediatR.Entity;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS_MediatR.Features.TransactionFeatures.Queries
{
    public class GetSucursalByIdQuery : IRequest<Sucursal>
    {
        public Guid Id { get; set; }
        public class GetSucursalByIdQueryHandler : IRequestHandler<GetSucursalByIdQuery, Sucursal>
        {
            private readonly ISucursalDbContext _context;
            public GetSucursalByIdQueryHandler(ISucursalDbContext context)
            {
                _context = context;
            }
            public async Task<Sucursal> Handle(GetSucursalByIdQuery query, CancellationToken cancellationToken)
            {
                var transcation = _context.Sucursal.Where(a => a.Id == query.Id && !a.IsSoftDeleted).FirstOrDefault();
                if (transcation == null) return null;
                return transcation;

            }
        }
    }
}