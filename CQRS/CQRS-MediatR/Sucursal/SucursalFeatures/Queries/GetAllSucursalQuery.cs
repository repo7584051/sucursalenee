﻿using CQRS_MediatR.Context;
using CQRS_MediatR.Entity;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS_MediatR.Features.TransactionFeatures.Queries
{
    public class GetAllSucursalQuery : IRequest<IEnumerable<Sucursal>>
    {
        public int PageNumer { get; set; }
        public int PageSize { get; set; } = 10;
        public class GetAllSucursalQueryHandler : IRequestHandler<GetAllSucursalQuery, IEnumerable<Sucursal>>
        {
            private readonly ISucursalDbContext _context;
            public GetAllSucursalQueryHandler(ISucursalDbContext context)
            {
                _context = context;
            }
            public async Task<IEnumerable<Sucursal>> Handle(GetAllSucursalQuery query, CancellationToken cancellationToken)
            {
                var startIndex = (query.PageNumer - 1) * query.PageSize;
                var sucursalList = await _context.Sucursal.Skip(startIndex).Take(query.PageSize).Where(s=> !s.IsSoftDeleted).ToListAsync();
               
                return sucursalList.AsReadOnly();
            }
        }
    }
}
