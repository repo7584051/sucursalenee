﻿using CQRS_MediatR.Context;
using CQRS_MediatR.Entity;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CQRS_MediatR.Features.TransactionFeatures.Commands
{
    public class CreateSucursalCommand : IRequest<string>
    {
        public string NombreSucursal { get; set; }
        public string NombreAdministrador { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string Fax { get; set; }
        public int NumeroPedidoMes { get; set; }
        public class CreateProductCommandHandler : IRequestHandler<CreateSucursalCommand, string>
        {
            private readonly ISucursalDbContext _context;
            public CreateProductCommandHandler(ISucursalDbContext context)
            {
                _context = context;
            }
            public async Task<string> Handle(CreateSucursalCommand command, CancellationToken cancellationToken)
            {
                var validator = new CreateSucursalCommandValidator(_context);
                var validatorReult = await validator.ValidateAsync(command);
                if (!validatorReult.IsValid) { return validatorReult.Errors.FirstOrDefault().ErrorMessage; }
                var suc = new Sucursal();
                suc.NombreSucursal = command.NombreSucursal;
                suc.NombreAdministrador = command.NombreAdministrador;
                suc.Telefono = command.Telefono;
                suc.Direccion = command.Direccion;
                suc.Fax = command.Fax;
                suc.NumeroPedidoMes = command.NumeroPedidoMes;
                _context.Sucursal.Add(suc);
                await _context.SaveChangesAsync();
                return suc.Id.ToString();
            }
        }
    }
}
