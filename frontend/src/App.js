import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Table,
  TableContainer,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
  Modal,
  Button,
  TextField,
} from "@material-ui/core";
import { Edit, Delete } from "@material-ui/icons";
import axios from "axios";
import "./App.css";

import { NumericFormat } from 'react-number-format';

const baseUrl = "https://localhost:44396/api/Sucursal";
const useStyles = makeStyles((theme) => ({
  modal: {
    position: "absolute",
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
  },
  iconos: {
    cursor: "pointer",
  },
  inputMaterial: {
    width: "100%",
  },
}));

function App() {
  const styles = useStyles();
  const [modalEliminar, setModalEliminar] = useState(false);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [data, setData] = useState([]);
  const [consolaSeleccionada, setConsolaSeleccionada] = useState({
    nombreSucursal: "",
    nombreAdministrador: "",
    telefono: "",
    direccion: "",
    Fax: "",
    numeroPedidoMes: 0,
  });
  useEffect(() => {
    peticionGetPuesto();
  }, []);
  const peticionGetPuesto = async () => {
    await axios
      .get(baseUrl + "?pageNumer=1")
      .then((response) => {
        setData(response.data);
      })
      .catch(() => {
        alert("No se pudo hacer la peticion");
      });
  };

  const abrirCerrarDelete = () => {
    setModalEliminar(!modalEliminar);
  };
  const abrirCerrarCreate = () => {
    setModalInsertar(!modalInsertar);
  };
  const abrirCerrarUpdate = () => {
    setModalEditar(!modalEditar);
  };

  const peticionDelete = async () => {
    await axios
      .delete(baseUrl + "/" + consolaSeleccionada.id)
      .then((response) => {
        setData(
          data.filter((consola) => consola.id !== consolaSeleccionada.id)
        );
        abrirCerrarDelete();
      });
  };
  const peticionPost = async () => {
    await axios.post(baseUrl, consolaSeleccionada).then((response) => {
      setData(data.concat(response.data));
      abrirCerrarCreate();
    });
  };
  const peticionPut = async () => {
    consolaSeleccionada.puesto = null;
    await axios
      .put(baseUrl + "/" + consolaSeleccionada.id, consolaSeleccionada)
      .then((response) => {
        var dataNueva = data;
        dataNueva.map((consola) => {
          if (consolaSeleccionada.id === consola.id) {
            consola.nombreAdministrador =
              consolaSeleccionada.nombreAdministrador;
            consola.nombreSucursal = consolaSeleccionada.nombreSucursal;
            consola.telefono = consolaSeleccionada.telefono;
            consola.direccion = consolaSeleccionada.direccion;
            consola.fax = consolaSeleccionada.fax;
            consola.numeroPedidoMes = consolaSeleccionada.numeroPedidoMes;
          }
        });
        setData(dataNueva);
        abrirCerrarUpdate();
        setModalEditar(false);
      });
  };
  const handleChange = (e) => {
    const { name, value } = e.target;
    setConsolaSeleccionada((prevState) => ({
      ...prevState,
      [name]: value,
    }));
    console.log(consolaSeleccionada);
  };
  const seleccionarConsola = (consola, caso) => {
    setConsolaSeleccionada(consola);
    caso === "Editar" ? abrirCerrarUpdate() : abrirCerrarDelete();
  };

  const bodyEliminar = (
    <div className={styles.modal}>
      <p>
        Estás seguro que deseas eliminar la Sucursal{" "}
        <b>{consolaSeleccionada && consolaSeleccionada.nombreSucursal}</b> ?{" "}
      </p>
      <div align="right">
        <Button color="secondary" onClick={() => peticionDelete()}>
          Sí
        </Button>
        <Button onClick={() => abrirCerrarDelete()}>No</Button>
      </div>
    </div>
  );
  const bodyInsertar = (
    <div className={styles.modal}>
      <h3>Crear Sucursal</h3>
      <TextField
        name="nombreSucursal"
        className={styles.inputMaterial}
        label="NombreSucursal"
        onChange={handleChange}
      />
      <br />
      <TextField
        name="nombreAdministrador"
        className={styles.inputMaterial}
        label="NombreAdministrador"
        onChange={handleChange}
      />
      <br />
      <TextField
        name="telefono"
        className={styles.inputMaterial}
        label="Telefono"
        onChange={handleChange}
      />
      <br />
      <TextField
        name="direccion"
        className={styles.inputMaterial}
        label="Direccion"
        onChange={handleChange}
      />
      <br />
      <TextField
        name="fax"
        className={styles.inputMaterial}
        label="Fax"
        onChange={handleChange}
      />
      <br />
      <TextField
        name="numeroPedidoMes"
        className={styles.inputMaterial}
        label="NumeroPedidoMes"
        onChange={handleChange}
      />
      <br />

      <br />
      <div align="right">
        <Button color="primary" onClick={() => peticionPost()}>
          Insertar
        </Button>
        <Button onClick={() => abrirCerrarCreate()}>Cancelar</Button>
      </div>
    </div>
  );
  const bodyEditar = (
    <div className={styles.modal}>
      <h3>Editar</h3>
      <TextField
        name="nombreAdministrador"
        className={styles.inputMaterial}
        label="NombreAdministrador"
        onChange={handleChange}
        value={consolaSeleccionada && consolaSeleccionada.nombreAdministrador}
      />
      <br />
      <TextField
        name="nombreSucursal"
        className={styles.inputMaterial}
        label="NombreSucursal"
        onChange={handleChange}
        value={consolaSeleccionada && consolaSeleccionada.nombreSucursal}
      />
      <br />
      <TextField
        name="telefono"
        className={styles.inputMaterial}
        label="Telefono"
        onChange={handleChange}
        value={consolaSeleccionada && consolaSeleccionada.telefono}
      />
      <br />
      <TextField
        name="direccion"
        className={styles.inputMaterial}
        label="Direccion"
        onChange={handleChange}
        value={consolaSeleccionada && consolaSeleccionada.direccion}
      />
      <br />
      <TextField
        name="fax"
        className={styles.inputMaterial}
        label="Fax"
        onChange={handleChange}
        value={consolaSeleccionada && consolaSeleccionada.fax}
      />
      <br />
      <label className={styles.inputMaterial}>Salario:</label>
      <NumericFormat
        name="numeroPedidoMes"
        className={styles.inputMaterial}
        label="NumeroPedidoMes"
        onChange={handleChange}
        value={consolaSeleccionada && consolaSeleccionada.numeroPedidoMes}
      />{" "}
      <br />
      <br />
      <div align="right">
        <Button color="primary" onClick={() => peticionPut()}>
          Editar
        </Button>
        <Button onClick={() => abrirCerrarUpdate()}>Cancelar</Button>
      </div>
    </div>
  );
  return (
    <div className="App">
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Nombre Administrador</TableCell>
              <TableCell>Nombre Sucursal</TableCell>
              <TableCell>Telefono</TableCell>
              <TableCell>Direccion</TableCell>
              <TableCell>Fax</TableCell>
              <TableCell>Numero pedido por mes</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((consola) => (
              <TableRow key={consola.id}>
                <TableCell>{consola.nombreAdministrador}</TableCell>
                <TableCell>{consola.nombreSucursal}</TableCell>
                <TableCell>{consola.telefono}</TableCell>
                <TableCell>{consola.direccion}</TableCell>
                <TableCell>{consola.fax}</TableCell>
                <TableCell>{consola.numeroPedidoMes}</TableCell>
                <TableCell>
                  <Edit
                    className={styles.iconos}
                    onClick={() => seleccionarConsola(consola, "Editar")}
                  />
                  &nbsp;&nbsp;&nbsp;
                  <Delete
                    className={styles.iconos}
                    onClick={() => seleccionarConsola(consola, "Eliminar")}
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Modal open={modalInsertar} onClose={abrirCerrarCreate}>
        {bodyInsertar}
      </Modal>

      <Modal open={modalEditar} onClose={abrirCerrarUpdate}>
        {bodyEditar}
      </Modal>
      <Modal open={modalEliminar} onClose={abrirCerrarDelete}>
        {bodyEliminar}
      </Modal>
      <br />
      <Button onClick={() => abrirCerrarCreate()}>Crear New</Button>
      <br />
      <br />
    </div>
  );
}

export default App;
